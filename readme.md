# Integrio Frontend Task Assignment

---

#### Hello dear participant 🖐

##### In this assignment, you need to show your skills in web development while creating a described _ReactJS_ application with the following features listed below.

### Task Description

This application has features like university search, showing user information, and creating users. These features are aimed to test your knowledge on creating react components with required features and required backend communications. You are going to use 2 different public apis for this task. One of them to get university informations, and oter one is to gather 
user informations.

- Users are able to search universities on the `university search page` based on selected query parameters.
- Users are able to see random user information when clicking the button or avatar.
- In `create user page` users can fill out the form and submit it.
- In the `users filter page`, random 100 users' info should be filterable as live search.

### Task Requirements

- Use Functional Components

- #### University Search Page

  - 1- Create a `UniversityFilter` component for university search. It is your choice to select filter parameters (at least 3) from API response parameters (can be found in the [doc](https://github.com/Hipo/university-domains-list#readme)).
  - 2- Send the filter data as search-query to the Public API.
  - 3- List the results.

    Public API: universities.hipolabs.com/
    Documentation: https://github.com/Hipo/university-domains-list#readme

- #### User Search Page

  - 1- Retrieve random 100 users as `?results=100`.
  - 2- Create a `UserFilter` component for user search from retrieved 100 users. Filter parameters can be `email` and `gender`. Email filter should be a text field,and
should filter the results in realtime, which means, according to written text in the email search bar, rendered users on the page should change. Both filters should work
coordinately.
  - 3 - List the filtered results.
  - 4 - While listing results, add pagination (page's up-limit is 50 items).

    Public API: https://randomuser.me/api/?results=100
    Documentation: https://randomuser.me/documentation
     
    **NOTE**: 
      - Pay attention to the difference between `University Search Page` and `User Search Page`. While `UniversityFilter` creates query parameter for get request, `UserFilter` filters the same request response inside the component.
      - `UniversityFilter` does not need to be a live search (submitting a filter form is enough). However, `UserFilter` is a live search so needs to show current results in any change.

- #### Profile Popup

  - Create a `UserDetailModal` popup component and when its opened show user info from `https://randomuser.me/api/`
    Public API: https://randomuser.me/api/
    Documentation: https://randomuser.me/documentation

- #### Create User Page

  - Build a `UserCreateForm` component with body parameters (name,lastName,email,gender) and post it to `https://randomuser.me/api/` then handle and log the error _(because API does not have POST request end-point)_.

- #### Styling
  - Styling depends on your imagination for this task
  - No need to be detailed styling but it's great to have class names that are well-named and well structured
  - Prefer separated `.scss` files rather than _in component styling (styled components)_

### Task Deadlines

- starts from **the mail received date**
- ends in **7** days

### Submit The Task

- You need to create new github or gitlab repository and push the code there.

  - **NOTE:** Initializing the repository before finishing the task and pushing commits during development indicates your development process better.

- When the task is ready, simply share the repository link with us.

---

Do not hesitate to contact with us 🙂

###### Good luck! 🍀

###### _The Integrio Team_
